/**
 * Created by hanni on 10/20/16.
 */

function getPostByIdBt() {
    document.getElementById('dopHtml').innerHTML = getPostHtml;
}

function updatePostBt() {
    document.getElementById('dopHtml').innerHTML = updateForm;
}

function deletePostBt() {
    document.getElementById('dopHtml').innerHTML = deletePostHtml;
}

function addNewPostBt() {
    document.getElementById('dopHtml').innerHTML = addPostHtml;
}

function addingData() {
    var obj = {};

    obj.Author = document.getElementById('author').value;
    obj.Title = document.getElementById('title').value;
    obj.Text = document.getElementById('text').value;

    addPost(obj, function (element) {
        console.log(element);
    });

    alert('Added new post!');

    document.getElementById('dopHtml').innerHTML = ``;
}

function updatedData() {
    var obj = {};

    obj.EntryId = document.getElementById('idPost').value;
    obj.Author = document.getElementById('author').value;
    obj.Text = document.getElementById('text').value;
    obj.Title = document.getElementById('title').value;

    updatePost(obj, function (element) {
        console.log(element);
    });

    alert('Data updated!');

    document.getElementById('dopHtml').innerHTML = `<p id="postViews"></p>`;
    getPostById(obj.EntryId);
}

function deletedData() {
    var obj = {};

    obj.EntryId = document.getElementById('idForfunc').value;

    deletePost(obj, function (element) {
        console.log(element);
    });

    document.getElementById('dopHtml').innerHTML = ``;
}

// function templateToHtml(data) {
//     var template, newTemplate, data, keys, i, j, result;
//
//     template = document.getElementById('GetAllPostsTemplate').innerHTML;
//     newTemplate = ``;
//
//
//
//     for (i = 0; i < data.length; i++) {
//         var objData = data[i];
// // console.log(objData);
//         keys = Object.keys(data[i]);
//         j = 0;
//         j < keys.length;
//
//         newTemplate += template.replace("{{" + keys[j] + "}}", objData[keys[j]]).replace("{{" + keys[j + 1] + "}}", objData[keys[j + 1]]);
//
//     }
//     result = document.getElementById('getAllPosts');
//     result.innerHTML = newTemplate;
//     document.result.appendChild(result);
//
//
// };


(function () {

        template = document.getElementById('GetAllPostsTemplate').innerHTML,
        result = document.getElementById('getAllPosts'),
        newTemplate = '',
        attachTemplateToData;

    function attachTemplateToData(template, data) {
        var i = 0,
            len = data.length,
            attach = '';


        var template, newTemplate, data, keys, key, reg, i, j, result;


        function replace(object) {

            for (key in object) {
                var newTemplate, key, reg;
                newTemplate = (newTemplate || template).replace("{{" + key + "}}", object[key]);
            }
            return newTemplate;

        }
        for (i = 0; i < data.length; i++) {
            attach += replace(data[i]);
        }
        return attach;

    };
        result.innerHTML = attachTemplateToData();

    // var button = document.getElementById('GetAllPostsTemplate');
    // button.onclick = function() {
    //     result.innerHTML = attachTemplateToData(template, data);
    //
    // };

})();
