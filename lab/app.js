function dataSorting(arg) {
    var arrayOfStrings = []; //
    data.forEach(function(item) {
        arrayOfStrings.push(item.split(', '));
    });

    var arrayOfValues = [];

    for (var i = 0; i < arrayOfStrings.length; i++) {
        arrayOfValues.push(arrayOfStrings[i][arg]);
        arrayOfValues.sort();
    }
    var leastCommonValue = arrayOfValues[0],
        mostCommonValue = arrayOfValues[0],
        leastCommonValueIndex,
        leastCommonValueIndexArray = [];

    function getLeastCommonValue() {

        var frequency = Object.keys(arrayOfValues);

        for (j = 0; j < arrayOfValues.length + 1; j++) {
            if (arrayOfValues[j] !== arrayOfValues[j]) {
                frequency[arrayOfValues[j]] = 1;

            } else {
                frequency[arrayOfValues[j]] = (frequency[arrayOfValues[j]] || 0) + 1;

                if (frequency[arrayOfValues[j]] == 1) {
                    leastCommonValueIndex = frequency[arrayOfValues[j - 1]];
                    leastCommonValueIndexArray.push([leastCommonValueIndex, arrayOfValues[j - 1]]);
                }

            }

        }

        leastCommonValueIndexArray.shift();
        leastCommonValueIndexArray.sort(function(a, b) {
            return a[0] - b[0];
        });
        leastCommonValue = leastCommonValueIndexArray[0][1];
        mostCommonValue = leastCommonValueIndexArray[leastCommonValueIndexArray.length - 1][1];

        return ['Least frequent value: ' + leastCommonValue, 'Most frequent value: ' + mostCommonValue];

    }

    return getLeastCommonValue();
}
console.log(dataSorting(0));
console.log(dataSorting(1));
console.log(dataSorting(2));
console.log(dataSorting(3));
console.log(dataSorting(4));
console.log(dataSorting(5));
