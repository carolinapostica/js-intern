'use strict';

var object = {
    name: 'Jnago',
    greeting: 'Hello',
    place: 'Cave'
};

function forEach(object, callback) {

    var keys = Object.keys(object);
    for (var i = 0; i < keys.length; i++) {
        callback(object[keys[i]], keys[i]);
    }
}





function map(object, callback) {
    var object = {
        name: 'Jnago',
        greeting: 'Hello',
        place: 'Cave'
    };
    var keys = Object.keys(object);
    var arr = [];

    for (var i = 0; i < keys.length; i++) {
        arr.push(callback(object[keys[i]]));
    }
    return arr;

}

var checkMap = map(object, function(v) {

    return v.length;

});


function filter(object, callback) {
    var object = [1, -3, 2, 0, 4, -4, -12];
    var keys = Object.keys(object);
    var arr = [];

    for (var i = 0; i < object.length; i++) {

        if (callback(object[keys[i]], keys[i]) == true) {
            arr.push(object[keys[i]]);
        }
    }

    return arr;
}

var checkFilter = filter(object, function(value, index) {
    return value > 0;

});






function reduce(object, callback) {
    var object = [1, 15, 2, 0, 4, -8, -12];
    var keys = Object.keys(object);
    var sum = 0;
    for (var i = 0; i < object.length; i++) {
        sum = callback(object[keys[i]], sum);
    }
    return sum;
}

var checkReduce = reduce(object, function(value, sum) {

    return value + sum;

}, 0);




function some(object, callback) {
    var object = [1, -3, 2, 0, 4, 4, 12];
    var keys = Object.keys(object);
    var arr = [];

    for (var i = 0; i < object.length; i++) {
        if (callback(object[keys[i]], keys[i]) !== true) {
            continue;

        } else {
            return true;
        }
    }

    return false;
}

var checkSome = some(object, function(value) {
    return value < 0;

});



function every(object, callback) {
    var object = [1, 3, 2, 0, 4, 4, 12];
    var keys = Object.keys(object);
    var arr = [];
    for (var i = 0; i < object.length; i++) {
        if (callback(object[keys[i]], keys[i]) == true) {
            continue;

        } else {
            return true;
        }
    }

    return false;
}

var checkEvery = every(object, function(v) {
    return v > 0;

});


var buttonEvery = document.querySelector('button#every');
var buttonSome = document.querySelector('button#some');
var buttonFilter = document.querySelector('button#filter');
var buttonMap = document.querySelector('button#map');
var buttonReduce = document.querySelector('button#reduce');
var buttonForEach = document.querySelector('button#forEach');

var code = document.querySelector('code');
buttonEvery.onclick = function() {
    code.innerHTML = every;
    alert(checkEvery);

};

buttonSome.onclick = function() {
    code.innerHTML = some;
    alert(checkSome);

};

buttonFilter.onclick = function() {
    code.innerHTML = filter;
    alert(checkFilter);

};

buttonMap.onclick = function() {
    code.innerHTML = map;
    alert(checkMap);

};

buttonReduce.onclick = function() {
    code.innerHTML = reduce;
    alert(checkReduce);

};

buttonForEach.onclick = function() {
    code.innerHTML = forEach;
    return forEach(object, function(v, i) {
        return alert(i + ': ' + v);
    });

};
